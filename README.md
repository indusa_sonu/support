# Support Team - Project

Welcome to the GitLab Support team project! 

We use this space as a team issue tracker, check out our [open issues](https://gitlab.com/gitlab-com/support/issues) or create a [new issue](https://gitlab.com/gitlab-com/support/issues/new).

Support team resources:
 * Handbook: https://about.gitlab.com/handbook/support/
 * Support knowledge base: https://about.gitlab.com/handbook/support/knowledge-base/
 * GitLab University: https://university.gitlab.com/support/
 * GitLab docs: http://docs.gitlab.com/
 * Team timezones: https://timezone.io/team/gitlab-support